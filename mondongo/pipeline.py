#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
from scrapy.exceptions import DropItem

from mondongo import flags


class SpoyderPipeline():
    """Revisa que un ítem no este repetido y lo guarda en la base de datos

    Para activar este Pipeline se debe editar la configuración del proyecto de
    scrapy con lo siguiente:

        ITEM_PIPELINES = {
            'mondongo.pipeline.SpoyderPipeline': 300,
        }
    """

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(flags.MONGO_URI)
        self.db = self.client["spoyderman_" + spider.name]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        if not self.db["records"].find_one(dict(item)):
            self.db["records"].insert_one(dict(item))
            return item
        else:
            raise DropItem("Duplicate item found: %s" % item)
