#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import json
import os


class BaseExporter():
    formatn = "BASE"

    def __init__(self, outputdir):
        self.output = os.path.expanduser(outputdir)
        self.fileopened = False
        self.ofile = None

    def __del__(self):
        self.close()

    def extension(self):
        """ Indica el formato sobre el cual trabaja este exportador
        """
        return self.formatn

    def filename(self, name):
        """ indica el nombre del archivo para la exportación
        """
        self.output = os.path.join(self.output, name)

    def close(self):
        """ Cierra el archivo
        """
        if self.fileopened:
            self.ofile.close()

    def insert(self, collection):
        """ Inserta la coleccion en el archivo CSV
        """
        raise NotImplementedError()  # pragma: no cover


class CSVExporter(BaseExporter):
    formatn = "CSV"

    def __init__(self, outputdir):
        super(CSVExporter, self).__init__(outputdir)
        self.csv = None

    def insert(self, collection):
        """ Inserta la coleccion en el archivo CSV
        """
        # Elimina la llave _id
        del collection["_id"]

        if not self.fileopened:
            self.fileopened = True
            # configura el archivo
            self.ofile = open(self.output, "w")
            self.csv = csv.DictWriter(
                self.ofile, fieldnames=collection.keys(),
                quoting=csv.QUOTE_MINIMAL)
            self.csv.writeheader()

        # inserta el documento en el archivo CSV
        self.csv.writerow(collection)


class TSVExporter(BaseExporter):
    formatn = "TSV"

    def __init__(self, outputdir):
        super(TSVExporter, self).__init__(outputdir)
        self.csv = None

    def insert(self, collection):
        # Elimina la llave _id
        del collection["_id"]

        if not self.fileopened:
            self.fileopened = True
            # configura el archivo
            self.ofile = open(self.output, "w")
            self.csv = csv.DictWriter(
                self.ofile, fieldnames=collection.keys(),
                quoting=csv.QUOTE_MINIMAL, delimiter='\t')
            self.csv.writeheader()

        # inserta el documento en el archivo CSV
        self.csv.writerow(collection)


class JSONExporter(BaseExporter):
    formatn = "JSON"

    def insert(self, collection):
        # Elimina la llave _id
        del collection["_id"]

        if not self.fileopened:
            self.fileopened = True
            # configura el archivo
            self.ofile = open(self.output, "w")

        json.dump(collection, self.ofile, ensure_ascii=False)


class DummyExporter(BaseExporter):
    formatn = "Dummy"

    def insert(self, collection):
        pass
