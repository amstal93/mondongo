Changelog
=========


(unreleased)
------------

New
~~~
- Crea reportes de coverage como html. [Jorge Araya Navarro]
- Carga el archivo léame con descripción larga. [Jorge Araya Navarro]
- Incorpora archivo de setuptools y modifica comando para CI. [Jorge
  Araya Navarro]
- Habilita la descarga de los artefactos generados por nosetest. [Jorge
  Araya Navarro]

  Necesito examinar el coverage del proyecto
- Implementa mecanismo de registro para exportadores. [Jorge Araya
  Navarro]

  Elimina duplicación y facilita la escritura de código con un pseudo switch-case
- Agrega pruebas de integración para los nuevos exportadores. [Jorge
  Araya Navarro]
- Implementa exportador para TSV y JSON. [Jorge Araya Navarro]
- Agrega archivo leame. [Jorge Araya Navarro]

  Explica de qué trata este proyecto y muestra la salud del mismo según el sistema de Integración
  Continua de Gitlab
- Implementa pruebas unitarias y de integración. [Jorge Araya Navarro]

  Mide la salud del proyecto
- Lista todos los requerimientos del proyecto. [Jorge Araya Navarro]
- Implementa la herramienta mondongo 🎉 [Jorge Araya Navarro]

Changes
~~~~~~~
- Coloca el mensaje de error dentro de `sys.exit` [Jorge Araya Navarro]
- Actualiza el archivo leame. [Jorge Araya Navarro]
- Mueve archivos para formar un modulo Python. [Jorge Araya Navarro]

  Esta es la estructura correcta de proyectos de módulos Python.
- Relicenciamiento a MIT. [Jorge Araya Navarro]
- Corre todas las pruebas en la prueba de integración. [Jorge Araya
  Navarro]

  Elimina el falso positivo de bajo coverage
- Elimina menciones de los formatos de exportacion. [Jorge Araya
  Navarro]
- Ignora los resultados del "test coverage" [Jorge Araya Navarro]
- Refactoriza funciones y clases a archivos separados. [Jorge Araya
  Navarro]

  Mejora la legibilidad del script principal y permite integrar mejores pruebas

Fix
~~~
- Instala mongomock con pip. [Jorge Araya Navarro]

  evita que setuptools instale una versión anterior de mongomock
- Fija la versión correcta de Mongomock. [Jorge Araya Navarro]
- Revierte cambio "Arreglar error `count() takes exactly one argument (0
  given)`" [Jorge Araya Navarro]
- Carga la información en la base de datos correcta. [Jorge Araya
  Navarro]

  los nombres de la bases de datos ahora son `spoyderman_`
- Arreglar error `count() takes exactly one argument (0 given)` [Jorge
  Araya Navarro]
- Llama a métodos de SpoyderPipeline. [Jorge Araya Navarro]

  Simula la ejecución de los métodos para el contexto de la prueba de integración
- Actualiza nombre de importación en prueba unitaria. [Jorge Araya
  Navarro]
- Inicializa variable que guarda el formato del exportador. [Jorge Araya
  Navarro]

  Todos los exportadores se estaban reportando con formato "CSV"
- Usa `sys.exit` en lugar de `exit` [Jorge Araya Navarro]

Other
~~~~~
- Revert "fix: Fija la versión correcta de Mongomock" [Jorge Araya
  Navarro]

  This reverts commit f3b15d99d419d5c01f1972d8957424cd8b32cd5c.
- Revert "fix: Inicializa variable que guarda el formato del exportador"
  [Jorge Araya Navarro]

  This reverts commit 393d540f605cb0527964a4c52a3d2cfdc9f08b23.


