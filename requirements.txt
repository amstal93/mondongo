astroid==1.5.3
autopep8==1.3.3
coverage==4.4.1
importmagic==0.1.7
isort==4.2.15
jedi==0.11.0
lazy-object-proxy==1.3.1
mccabe==0.6.1
mock==2.0.0
mongomock==3.8.0
nose==1.3.7
parso==0.1.0
pbr==3.1.1
pycodestyle==2.3.1
pylint==1.7.4
pymongo==3.5.1
rope==0.10.7
sentinels==1.0.0
six==1.11.0
stevedore==1.27.1
tqdm==4.19.4
virtualenv==15.1.0
virtualenv-clone==0.2.6
virtualenvwrapper==4.8.2
wrapt==1.10.11
yapf==0.19.0
